path "secrets/data/demo/postgres/username" {
  capabilities = ["read", "list"]
}
path "secrets/data/demo/postgres/password" {
  capabilities = ["read", "list"]
}
