#!/bin/bash -x
######
# This script sets your required service account policy bindings so you can use the Terraform in this project.
# Example call:
# bash ./iam_policy_binding.sh -p jsandlin-c9fe7132 -a jsandlin-admin
# REF
# https://cloud.google.com/iam/docs/understanding-roles#basic
######
# while getopts a:p: flag
# do
#     case "${flag}" in
#         p) GCP_PROJECT=${OPTARG};;
#         a) SERVICE_ACCOUNT_NAME=${OPTARG};;
#     esac
# done
# $ gcloud iam list-testable-permissions //cloudresourcemanager.googleapis.com/projects/jsandlin-c9fe7132 --filter="customRolesSupportLevel!=NOT_SUPPORTED"
GCP_PROJECT="jsandlin-c9fe7132"
SERVICE_ACCOUNT_NAME="apigee-sa"
ROLE_TITLE=$SERVICE_ACCOUNT_NAME

sa_exists=`gcloud iam service-accounts list | grep $SERVICE_ACCOUNT_NAME | wc -l`

if [[ $sa_exists -eq "0" ]]
then
  gcloud iam service-accounts create $SERVICE_ACCOUNT_NAME \
    --description="The root level service account" \
    --display-name="$SERVICE_ACCOUNT_NAME"
fi

# # CREATE & DOWNLOAD KEYFILE
# gcloud iam service-accounts keys create ~/.gcp/$SERVICE_ACCOUNT_NAME.json \
#     --iam-account=$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com

# # CHECK IF ROLE EXISTS.
# if [ `gcloud iam roles list --project=$GCP_PROJECT --filter=$SERVICE_ACCOUNT_NAME | wc -l` -gt 0 ]
# then
#   echo Y | gcloud iam roles update $ROLE_TITLE --project=$GCP_PROJECT --file=./apigee-sa_role.yml
# else
#   # CREATE CUSTOM ROLE
#   gcloud iam roles create $ROLE_TITLE --project=$GCP_PROJECT --file=./apigee-sa_role.yml
# fi
# gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="projects/$GCP_PROJECT/roles/$ROLE_TITLE"

declare -a RoleArray=(
#   "accesscontextmanager.policyAdmin" # Full access to policies, access levels, and access zones
#   "cloudkms.admin" # Provides full access to Cloud KMS resources, except encrypt and decrypt operations. https://cloud.google.com/iam/docs/understanding-roles#cloud-kms-roles
#   "cloudkms.cryptoKeyEncrypterDecrypter" # Provides ability to use Cloud KMS resources for encrypt and decrypt operations only.
#   #"cloudresourcemanage"
#   "dns.admin" # Error finding zone. Skipping cleanup. Encountered error finding managed zone: <HttpError 403 when requesting https://dns.googleapis.com/dns/v1/projects/jsandlin-c9fe7132/managedZones?dnsName=vault.in-betweener.com.&alt=json returned "Forbidden". Details: "[{'message': 'Forbidden', 'domain': 'global', 'reason': 'forbidden'}]">
#   "compute.admin" # I don't like this; too open for privs. Can we trim?
#   "compute.beta"
#   "compute.networkAdmin"
#   "compute.networkViewer"
#   "compute.viewer"
#   "compute.securityAdmin"
#   "compute.instanceAdmin"
#   "compute.serviceAgent"
#   "compute.regionInstanceGroupManager"
#   "storage.buckets.get" # - required for apigee-sa in Vault setup. # Cannot provide for this resource.
#   "storage.admin"
#   "compute.instanceAdmin.v1" # Permissions to create, modify, and delete virtual machine instances. This includes permissions to create, modify, and delete disks, and also to configure Shielded VM settings.
#   "compute.networkAdmin" # Permissions to create, modify, and delete networking resources, except for firewall rules and SSL certificates.
#   "container.clusterAdmin" # Required to manage cluster
#   "iam.serviceAccountAdmin" # Required to create service account to manage cluster
#   "iam.serviceAccountUser" # Why do I need admin and user?
#   "monitoring.admin" # monitoring.googleapis.com
#   "iam.roleAdmin" # Provides access to all custom roles in the project.
#   "iam.serviceAccountTokenCreator" # Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  # FOR THE SAKE OF APIGEE
  # https://cloud.google.com/apigee/docs/hybrid/v1.10/install-service-accounts.html#gcloud
  "storage.objectAdmin"
  "logging.logWriter"
  "apigeeconnect.Agent"
  "monitoring.metricWriter"
  "apigee.synchronizerManager"
  "apigee.analyticsAgent"
  "apigee.runtimeAgent"
  "apigee.admin"
  

)
for r in ${RoleArray[@]}; do
  gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/$r"
done

# for r in ${RoleArray[@]}; do
#   gcloud projects remove-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/$r"
# done

# ##################################
# # Now let's echo out our policies for this account.
# ##################################
# gcloud projects get-iam-policy  $GCP_PROJECT \
#     --flatten="bindings[].members" \
#     --format="table(bindings.role)" \
#     --filter="bindings.members:$SERVICE_ACCOUNT_NAME"

# gcloud projects get-iam-policy  $GCP_PROJECT --flatten="bindings[].members" --format="table(bindings.role)" --filter="bindings.members:$SERVICE_ACCOUNT_NAME"


# # PUT KV IN VAULT & SET PRIVS
# vault kv put -format=json secrets/gcp/apigee-sa @/Users/jsandlin/.gcp/apigee-sa.json
# cat ~/.gcp/apigee-sa.json | vault kv put secrets/gcp/apigee-sa json=-
# vault policy write apigee-sa-ro ./apigee-sa_vault_policy.hcl
# vault write auth/jwt/role/apigee-sa-ro @apigee-sa_vault_role.hcl



# # 
# gcloud projects add-iam-policy-binding "$PROJECT" \
#   --member="serviceAccount:$SERVICE_ACCOUNT_NAME" \
#   --role="roles/owner"