{
  "role_type": "jwt",
  "policies": ["gl-187"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_audiences": [
    "https://gitlab.com"
  ],
  "bound_claims": {
    "project_id": [
      "50761292"
    ]
  }
}
