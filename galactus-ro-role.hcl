{
  "role_type": "jwt",
  "policies": ["galactus-ro"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_audiences": [
    "https://gitlab.com"
  ],
  "bound_claims": {
    "project_id": [
      "30433120",
      "50265899"
    ],
    "user_login": "jsandlin"
  }
}
