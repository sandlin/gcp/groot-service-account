#!/bin/bash
vault policy delete gl-187
vault policy write gl-187 ./gl-187-policy.hcl
vault policy read gl-187

vault delete auth/jwt/role/gl-187
vault write auth/jwt/role/gl-187 @gl-187-role.hcl
vault read auth/jwt/role/gl-187  