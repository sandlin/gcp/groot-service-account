This is the project which must be executed before running any automation.

This project will create the admin service account (Galactus), which will be used to manage from a high level across my projects.

This repo's commands shall be executed as the owner account of my project, as it creates the first service account.

The only GitLab project which can read this is https://gitlab.com/sandlin/gcp/sa-management


1. Create Service Account & grant privs.
   ```
   bash ./galactus_sa.sh
   ```



- [Bound claims GitLab uses](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/)
- [Convert to ID Tokens](https://docs.gitlab.com/ee/ci/secrets/convert-to-id-tokens.html)
- [ID Tokens - Details](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html)
---

## List Roles
```
$ vault list auth/jwt/role
Keys
----
demo-ro
galactus-ro
gke-deploy-env
```

## Show details of specific role
```
$ vault read auth/jwt/role/galactus-ro
Key                        Value
---                        -----
allowed_redirect_uris      <nil>
bound_audiences            [https://gitlab.com]
...
```
```
$ vault read auth/jwt/role/demo-ro
Key                        Value
---                        -----
allowed_redirect_uris      <nil>
bound_audiences            [https://gitlab.com]
bound_claims               map[project_id:[30561409 50265899]]
...
```
## Policy information
```
$ vault policy list
default
demo-ro
galactus-ro
root
```
```
$ vault policy read demo-ro
path "secrets/data/demo/postgres/username" {
  capabilities = ["read", "list"]
}
path "secrets/data/demo/postgres/password" {
  capabilities = ["read", "list"]
}
```

## Ensure policy & role is configured
```
$ ./demo-ro-manage.sh 
Success! Uploaded policy: demo-ro
```

## Get secret with details
### username
```
$ vault kv get secrets/demo/postgres/username      
=========== Secret Path ===========
secrets/data/demo/postgres/username

====== Metadata ======
Key              Value
---              -----
created_time     2021-11-10T22:24:13.298868424Z
deletion_time    n/a
destroyed        false
version          1

==== Data ====
Key      Value
---      -----
value    pguser
```

### galactus 
```
$ vault kv get secrets/gcp/galactus
====== Secret Path ======
secrets/data/gcp/galactus

====== Metadata ======
Key              Value
---              -----
created_time     2023-04-05T16:28:47.473813358Z
deletion_time    n/a
destroyed        false
version          36

==== Data ====
Key     Value
---     -----
json    {
  "type": "service_account",
  "project_id": "jsandlin-c9fe7132",
  ...
}
```

## Get secret value
```
$ vault kv get -field=value secrets/demo/postgres/username
pguser
```

```
$ vault kv get -field=json secrets/gcp/galactus
{
  "type": "service_account",
  "project_id": "jsandlin-c9fe7132",
  "private_key_id": "666",
  "private_key": "-----BEGIN PRIVATE KEY-----\nxxx\n-----END PRIVATE KEY-----\n",
  "client_email": "galactus@jsandlin-c9fe7132.iam.gserviceaccount.com",
  "client_id": "117955482288072837149",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com..."
}

```
