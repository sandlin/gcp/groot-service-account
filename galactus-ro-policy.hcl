path "secrets/data/gcp/galactus" {
  capabilities = ["read", "list"]
}
path "secrets/data/infracost/api_token" {
  capabilities = ["read", "list"]
}
path "secrets/data/gitlab/api_token_infracost" {
  capabilities = ["read", "list"]
}
