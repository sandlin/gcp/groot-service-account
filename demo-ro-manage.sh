#!/bin/bash

# This config is used for my vault_reader project in GitLab.
# https://gitlab.com/sandlin/examples/vault_reader

vault policy delete demo-ro
vault policy write demo-ro ./demo-ro-policy.hcl
vault policy read demo-ro

vault delete auth/jwt/role/demo-ro
vault write auth/jwt/role/demo-ro @demo-ro-role.hcl
vault read auth/jwt/role/demo-ro