#!/bin/bash
vault policy delete galactus-ro
vault policy write galactus-ro ./galactus-ro-policy.hcl
vault policy read galactus-ro

vault delete auth/jwt/role/galactus-ro
vault write auth/jwt/role/galactus-ro @galactus-ro-role.hcl
vault read auth/jwt/role/galactus-ro  